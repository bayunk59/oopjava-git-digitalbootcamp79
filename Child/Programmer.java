package oopJava.AccesModifier.Child;
import oopJava.AccesModifier.Parents.Person;

public class Programmer extends Person{
    public String technology;

    //constructor default milik child
    public Programmer(){
        super();
    }
    //constructor berparameter
    public Programmer(String name, String address, String technology){
        super(name, address); //milik parents
        this.technology = technology; //milik child
    }

    public void hacking(){
        System.out.println("I can hacking a website");
    }

    public void coding(){
        System.out.println("I can create a application using technology: "+ technology +".");
    }

    public void greeting(){
        //super keyword
        super.greeting(); //memanggil method greeting dari parents
        System.out.println("My job is a "+ technology +" programmer");
        }
    
}
