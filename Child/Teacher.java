package oopJava.AccesModifier.Child;
import oopJava.AccesModifier.Parents.Person;

public class Teacher extends Person{
    public String subject;

    //constructor default milik child
    public Teacher(){
        super();
    }
    //constructor berparameter
    public Teacher(String name, String address, String subject){
        super(name, address); //milik parents
        this.subject = subject; //milik child
    }

    public void teaching(){
        System.out.println("I can teach "+ subject +", So anyone how wants to learn can talk to me.");
    }

    //overriding method
    public void greeting(){
    System.out.println("Hello my name is "+ name +".");
    System.out.println("I come from "+ address +".");
    System.out.println("My job is a "+ subject +" teacher");
    }
}
