package oopJava.AccesModifier.Child;
import oopJava.AccesModifier.Parents.Person;

public class Doctor extends Person{
    public String specialist;
    
    //constructor default milik child Doctor
    public Doctor(){
        super();
    }
    //constructor berparameter
    public Doctor(String name, String address, String specialist){
        super(name, address); //milik parents
        this.specialist = specialist; //milik Doctor (child)
    }

    public void surgery() {
        System.out.println("I can surgery operation patients");
    } 

    public void greeting(){
        System.out.println("Hello my name is "+ name +".");
        System.out.println("I come from "+ address +".");
        System.out.println("My occupation is a "+ specialist +" doctor");
        }
}