package oopJava.AccesModifier;
import oopJava.AccesModifier.Child.Doctor;
import oopJava.AccesModifier.Child.Programmer;
import oopJava.AccesModifier.Child.Teacher;
import oopJava.AccesModifier.Parents.Person;

public class appMain {
    public static void main(String[] args){
        Person person1 = new Programmer("Rizky", "Bandung", ".Net Core");
        person1.greeting();
       
        System.out.println(((Programmer)person1).technology); // recasting, mengubah tipe data person1 dari child Person jadi child Programmer
        System.out.println("=============================");

        Person person2 = new Teacher("Joko", "Tegal", ".Matematika");
        Person person3 = new Doctor("Eko", "Surabaya", "Pedistrican");

        sayHello(person1);
        sayHello(person2);
        sayHello(person3);
    }

    static void sayHello (Person person){
        String message;
        if(person instanceof Programmer){
            Programmer programmer = (Programmer) person;
            message = "Hello "+ programmer.name +". Seorang programmer "+ programmer.technology +".";
        }else if(person instanceof Teacher){
            Teacher teacher = (Teacher) person;
            message = "Hello "+ teacher.name +". Seorang guru "+ teacher.subject +".";
        }else if(person instanceof Doctor){
            Doctor doctor = (Doctor) person;
            message = "Hello "+ doctor.name +". Seorang doctor "+ doctor.specialist +".";
        }else {
            message = "Hello, "+ person.name +".";
        }  
        System.out.println(message);

    }
}
